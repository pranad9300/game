import React, { useState } from "react";
import Box from "./Box";
import "./Css/Board.css";

function Board() {
  const arr = new Array(10);

  for (let i = 0; i < 10; i++) {
    arr[i] = new Array(10);
  }

  const [boxArr, setBoxArr] = useState(arr);
  const [turn, setTurn] = useState("red");
  const [emptyBoxes, setEmptyBoxes] = useState(100);
  const [winner, setWinner] = useState("");
  //checkingHorizantal Mehod
  const checkHorizantal = (check_array, row, col, check_for) => {
    let count_right = 0;
    let count_left = 0;
    let i;
    //checking horizantally right
    for (i = col + 1; i < 10; i++) {
      if (count_right === 4 || check_array[row][i] !== check_for) {
        break;
      } else if (check_array[row][i] === check_for) {
        count_right++;
      }
    }
    //checking horizantally left
    for (let i = col - 1; i >= 0; i--) {
      if (count_left === 4 || check_array[row][i] !== check_for) {
        break;
      } else if (check_array[row][i] === check_for) {
        count_left++;
      }
    }
    if (count_left + count_right + 1 === 4) {
      return true;
    } else {
      return false;
    }
  };

  //checkingVerticallMethod
  const checkVertical = (check_array, row, col, check_for) => {
    let count_above = 0;
    let count_below = 0;
    let i;
    //checking vertically below
    for (i = row + 1; i < 10; i++) {
      if (count_below === 4 || check_array[i][col] !== check_for) {
        break;
      } else if (check_array[i][col] === check_for) {
        count_below++;
      }
    }
    //checking verticall upwards
    for (let i = row - 1; i >= 0; i--) {
      if (count_above === 4 || check_array[i][col] !== check_for) {
        break;
      } else if (check_array[i][col] === check_for) {
        count_above++;
      }
    }
    if (count_above + count_below + 1 === 4) {
      return true;
    } else {
      return false;
    }
  };

  //checkingDiagonal Method
  const checkDiagonal = (check_array, row, col, check_for) => {
    let countDiagonal_above_right = 0;
    let countDiagonal_above_left = 0;
    let countDiagonal_below_right = 0;
    let countDiagonal_below_left = 0;
    let i;
    //checking diagonally_up_towards_right
    for (i = 1; i < 4; i++) {
      if (
        row - i < 0 ||
        col + i > 9 ||
        check_array[row - i][col + i] !== check_for
      ) {
        break;
      } else if (check_array[row - i][col + i] === check_for) {
        countDiagonal_above_right++;
      }
    }
    //checking diagonally_down_towards_left
    for (i = 1; i < 4; i++) {
      if (
        row + i > 9 ||
        col - i < 0 ||
        check_array[row + i][col - i] !== check_for
      ) {
        break;
      } else if (check_array[row + i][col - i] === check_for) {
        countDiagonal_below_left++;
      }
    }
    //checking diagonally_up_towards_left
    for (i = 1; i < 4; i++) {
      if (
        row - i < 0 ||
        col - i < 0 ||
        check_array[row - i][col - i] !== check_for
      ) {
        break;
      } else if (check_array[row - i][col - i] === check_for) {
        countDiagonal_above_left++;
      }
    }
    //checking diagonally_down_towards_right
    for (i = 1; i < 4; i++) {
      if (
        row + i > 9 ||
        col + i > 9 ||
        check_array[row + i][col + i] !== check_for
      ) {
        break;
      } else if (check_array[row + i][col + i] === check_for) {
        countDiagonal_below_right++;
      }
    }
    if (
      countDiagonal_below_left + countDiagonal_above_right + 1 === 4 ||
      countDiagonal_below_right + countDiagonal_above_left + 1 === 4
    ) {
      return true;
    } else {
      return false;
    }
  };

  //checkin if soemwone won or not
  const checkWin = (clickked_row, clicked_col, check_for) => {
    const check_array = { ...boxArr };
    //checking Horizantally
    const result_horizantal = checkHorizantal(
      check_array,
      clickked_row,
      clicked_col,
      check_for
    );
    // checking Vertically
    const result_vertical = checkVertical(
      check_array,
      clickked_row,
      clicked_col,
      check_for
    );
    //checking Diagonally
    const result_diagonal = checkDiagonal(
      check_array,
      clickked_row,
      clicked_col,
      check_for
    );
    //final condition for win check
    if (result_diagonal || result_horizantal || result_vertical) {
      setWinner(turn);
      setTurn("");
      const welcom_element = document.getElementById("welcome");
      const result_element = document.getElementById("result");
      welcom_element.innerHTML = "Congratulations!!!";
      result_element.innerHTML = `${winner} won the game.`;
    } else {
      //condition for draw
      if (emptyBoxes === 0 && winner === "") {
        setTurn("");
        const welcom_element = document.getElementById("welcome");
        const result_element = document.getElementById("result");
        welcom_element.innerHTML = "Sorry!!!";
        result_element.innerHTML = `Game is drawn.`;
      }
    }
  };

  //rebdering borad_boxes
  const borad_boxes = (row) => {
    let i = 0;
    let boxes = [];
    for (i = 0; i < 10; i++) {
      boxes[i] = (
        <Box
          row={row}
          col={i}
          setBoxArr={setBoxArr}
          boxArr={boxArr}
          turn={turn}
          setTurn={setTurn}
          checkWin={checkWin}
          emptyBoxes={emptyBoxes}
          setEmptyBoxes={setEmptyBoxes}
          winner={winner}
        />
      );
    }
    return boxes;
  };

  return (
    <div className="board-container">
      <div className="board">
        <div className="board-row">{borad_boxes(0)}</div>
        <div className="board-row">{borad_boxes(1)}</div>
        <div className="board-row">{borad_boxes(2)}</div>
        <div className="board-row">{borad_boxes(3)}</div>
        <div className="board-row">{borad_boxes(4)}</div>
        <div className="board-row">{borad_boxes(5)}</div>
        <div className="board-row">{borad_boxes(6)}</div>
        <div className="board-row">{borad_boxes(7)}</div>
        <div className="board-row">{borad_boxes(8)}</div>
        <div className="board-row">{borad_boxes(9)}</div>
      </div>
      <div>
        <h3 id="welcome">Welcome to the Game:</h3>
        <h3 id="result">Its {turn}'s turn now.</h3>
      </div>
    </div>
  );
}

export default Board;
