import React from "react";
import "./Css/Box.css";

function Box({
  row,
  col,
  setBoxArr,
  boxArr,
  turn,
  setTurn,
  checkWin,
  emptyBoxes,
  setEmptyBoxes,
  winner,
}) {
  const box_id = `box-${row}-${col}`;

  const handleBoxClick = (box_id) => {
    const newBoxArr = { ...boxArr };
    if (newBoxArr[row][col] === "X" || newBoxArr[row][col] === "O") {
      alert("Click on empty box!!!");
    } else if (winner !== "") {
      alert("Game has finished. If you want to play again refresh please.");
    } else {
      const newEmptyBoxes = emptyBoxes;
      setEmptyBoxes(newEmptyBoxes - 1);
      const box_el = document.getElementById(box_id);
      const element = document.createElement("div");
      if (turn === "red") {
        newBoxArr[row][col] = "X";
        element.style.backgroundColor = "red";
        box_el.appendChild(element);
        setBoxArr(newBoxArr);
        checkWin(row, col, "X");
        setTurn("blue");
      } else {
        newBoxArr[row][col] = "O";
        element.style.backgroundColor = "blue";

        box_el.appendChild(element);
        setBoxArr(newBoxArr);
        checkWin(row, col, "O");
        setTurn("red");
      }
    }
  };

  return (
    <div
      className="box"
      id={box_id}
      onClick={() => handleBoxClick(box_id)}
    ></div>
  );
}

export default Box;
